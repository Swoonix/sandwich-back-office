from rest_api.models import Profile, Trajet, Reservation, Evenement
from django.contrib.auth.models import User

# p = Profile(username='corentin', email='corentin@gmail.com', password='sandwich', phone="06.40.95.60.01")
# p.save()
# Profile.objects.create_user('nonsam', 'alber@gmail.com', 'sandwich')

# e = Evenement(name='Pink Paradise', lieu='Toulouse', description='Basé sur une imagination désinvolte, Pink Paradize est un événement transdisciplinaire autoproduit au coeur de la ville rose')
# e.save()
# t = Trajet(createur=Profile.objects.get(pk=1), nb_places=5, evenement=e)
# t.save()

# e = Evenement.objects.get(pk=1)
# t2 = Trajet(createur=Profile.objects.get(pk=2), nb_places=2, evenement=e)
# t2.save()

# r = Reservation(user=Profile.objects.get(pk=2), trajet=Trajet.objects.get(pk=1))
# r.save()

# print(Trajet.objects.get(pk=1).reservations.all())
# print(User.objects.get(pk=3).reservations.all())