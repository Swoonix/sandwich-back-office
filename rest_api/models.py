from django.db import models
from django.contrib.auth.models import AbstractUser

class Profile(AbstractUser):
    phone = models.CharField(max_length=20, blank=True)
    points = models.IntegerField()
    
class Evenement(models.Model):
    name = models.CharField(max_length=50)
    lieu = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    site_web = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return "%s se passant à %s" % (self.name, self.lieu)

class Trajet(models.Model):
    createur = models.ForeignKey(Profile, related_name='trajets', on_delete=models.CASCADE)
    nb_places = models.IntegerField()
    evenement = models.ForeignKey(Evenement, related_name='trajets', on_delete=models.CASCADE)
    lieu_arrive = models.CharField(max_length=50)
    heure_depart = models.CharField(max_length=50)
    avis = models.TextField(blank=True)

    def __str__(self):
        return "Trajet crée par %s" % (self.createur)

# Une reservation peut-être faîte par plusieurs utilisateurs (non-sam) et à plusieurs evenements 
class Reservation(models.Model):
    user = models.ForeignKey(Profile, related_name='reservations', on_delete=models.CASCADE)
    trajet = models.ForeignKey(Trajet, related_name='reservations', on_delete=models.CASCADE)
