from django.shortcuts import render
from rest_api.models import Trajet, Reservation, Evenement, Profile
from rest_api.serializers import ProfileSerializer, TrajetSerializer, ReservationSerializer, EvenementSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view


# GET (récupère la liste) 
# POST (créé)
class EvenementList(generics.ListCreateAPIView):
    queryset = Evenement.objects.all()
    serializer_class = EvenementSerializer

# GET (récupère)
# PUT (change)
# DELETE (supprime)
class EvenementDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Evenement.objects.all()
    serializer_class = EvenementSerializer

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

class TrajetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Trajet.objects.all()
    serializer_class = TrajetSerializer

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

class ProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

# POST
class UserCreation(generics.CreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

class TrajetCreation(generics.CreateAPIView):
    queryset = Trajet.objects.all()
    serializer_class = TrajetSerializer

class ReservationCreation(generics.CreateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
