from rest_framework import serializers
from rest_api.models import Trajet, Evenement, Reservation, Profile

class TrajetSerializer(serializers.ModelSerializer):
    createur_id = serializers.IntegerField()
    evenement_id = serializers.IntegerField()

    class Meta:
        model = Trajet
        depth = 2
        fields= ('pk', 'reservations', 'createur', 'nb_places', 'evenement_id', 'lieu_arrive', 'createur_id')
    
    def create(self, validated_data):
        c = Profile.objects.get(pk=validated_data['createur_id'])
        c.points = c.points + 100
        c.save()
        t = Trajet(nb_places=validated_data['nb_places'], 
                lieu_arrive=validated_data['lieu_arrive'], 
                createur=c,
                evenement=Evenement.objects.get(pk=validated_data['evenement_id']))
        t.save()
        return t

class EvenementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evenement
        depth = 2
        fields= ('pk', 'name', 'lieu', 'description', 'trajets')

class ReservationSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField()
    trajet_id = serializers.IntegerField()

    class Meta:
        model = Reservation
        fields = ('pk', 'user_id', 'trajet_id') 

    def create(self, validated_data):
        t = Trajet.objects.get(pk=validated_data['trajet_id'])
        t.nb_places = t.nb_places - 1
        t.save()
        r = Reservation(
            user=Profile.objects.get(pk=validated_data['user_id']),
            trajet=t)
        
        r.save()
        return r


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        #fields = ('username', 'password', 'points', 'phone', 'first_name', 'last_name', 'email', 'trajets')
        depth = 3
        fields = ('username', 'trajets', 'reservations', 'password', 'points', 'phone', 'first_name', 'last_name', 'email')
        extra_kwargs = {
          'phone': {'required': True},
          'first_name': {'required': True},
          'last_name': {'required': True},
          'email': {'required': True},
          'password': {'required': True}
        }


    def create(self, validated_data):
        p = Profile(
            username=validated_data['username'],
            phone=validated_data['phone'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        p.set_password(validated_data['password'])
        p.save()
        return p
