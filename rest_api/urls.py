from django.urls import path
from rest_api import views

from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path('get_token', obtain_jwt_token),
    path('evenements', views.EvenementList.as_view()),
    path('evenement/<int:pk>', views.EvenementDetail.as_view()),
    path('trajet/<int:pk>', views.TrajetDetail.as_view()),
    path('profil/<int:pk>', views.ProfileDetail.as_view()),
    path('create_trajet', views.TrajetCreation.as_view()),
    path('create_reservation', views.ReservationCreation.as_view()),
    path('create_user', views.UserCreation.as_view())
]
